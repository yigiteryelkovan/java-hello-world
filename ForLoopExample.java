public class ForLoopExample {
  public static void main(String[] args) {
	  System.out.println("Start For Loop");
	  for(int i=0; i<5; i++){
		  System.out.println((i+1)+". line");
	  }
    System.out.println("End For Loop");
  }
}
